package com.example.springboot;

import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;

@RestController
public class HelloController {

	@RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

	@RequestMapping("/getName")
	public String getName() {
		return "NameFromCall" ;
	}

	@RequestMapping(value = "/feedback", method = RequestMethod.POST)
	public void response(@RequestBody String feedback) {
        LocalDateTime responseTime = LocalDateTime.now();
        LocalDateTime requestTime = LocalDateTime.parse(feedback.split("::")[1].trim());
        Duration duration = Duration.between(requestTime, responseTime);

        System.out.println("Am primit feedback de la MVC: la ora: " + responseTime + "; " + feedback
                +"; call-ul a durat " + duration.toMillis() + " ms");
    }

}
